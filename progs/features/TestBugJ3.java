class TestBugJ3 {
    public static void main(String[] args) {
        System.out.println(new Test().f());
    }
}

class Test {
    public int f() {
        int x;
        int y;
        int result;

        x = 5;
        y = 3;
        x += y; // Should modify x, but the bug will not modify x

        result = x;
        return result;
    }
}


