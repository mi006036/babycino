class TestBugJ2 {
    public static void main(String[] args) {
        System.out.println(new Test().f());
    }
}

class Test {
    public int f() {
        int x;
        int y;
        int result;

        x = 5;
        y = 3;
        x += y; // Incorrect assignment, should assign y to x instead

        result = x;
        return result;
    }
}
